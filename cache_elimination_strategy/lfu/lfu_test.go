/*
 * @Author: crissu 13750782397@163.com
 * @Date: 2023-07-30 23:27:02
 * @LastEditors: crissu 13750782397@163.com
 * @LastEditTime: 2023-07-31 00:41:23
 * @FilePath: /cache_elimination_strategy/lfu/lfu.go
 * @Description:  LFU（Least Frequently Used）
 */
package lfu

import (
	"fmt"
	"testing"
)

func TestSet(t *testing.T) {
	cache := New(24, nil)
	cache.DelOldest()
	cache.Set("k1", 1)
	fmt.Println("key:k1, value:", cache.Get("k1"))

}
