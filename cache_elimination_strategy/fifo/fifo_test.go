/*
 * @Author: crissu 13750782397@163.com
 * @Date: 2023-07-30 16:10:38
 * @LastEditors: crissu 13750782397@163.com
 * @LastEditTime: 2023-07-30 17:07:34
 * @FilePath: /cache_elimination_strategy/fifo/fifo.go
 * @Description: 缓存策略 fifo 先进先出
 */
package fifo

import (
	"fmt"
	"testing"
)

func TestSetGet(t *testing.T) {

	cache := New(24, nil)
	cache.DelOldest()
	cache.Set("k1", 1)
	v := cache.Get("k1")
	cache.Del("k1")
	fmt.Println("v:", v)
	fmt.Println(cache.Len())
}

func TestOnEvicted(t *testing.T) {

	keys := make([]string, 0, 8)
	onEvicted := func(key string, value interface{}) {
		keys = append(keys, key)
	}
	cache := New(16, onEvicted)

	cache.Set("k1", 1)
	cache.Set("k2", 2)
	cache.Get("k1")
	cache.Set("k3", 3)
	cache.Get("k1")
	cache.Set("k4", 4)

	cache.DelOldest()
	cache.Del("k2")

	expected := []string{"k1", "k2"}
	fmt.Println("expected:", expected)
	fmt.Println("keys:", keys)
	fmt.Println("cache.Len():", cache.Len())
}
