module ache_elimination_strategy

go 1.17

require github.com/go-playground/locales v0.14.1

require (
	github.com/smartystreets/goconvey v1.8.1 // indirect
	golang.org/x/mod v0.9.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/tools v0.7.0 // indirect
)
