module gin

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/swaggo/gin-swagger v1.3.0
)
