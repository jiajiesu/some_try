package gin

import (
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"net/http"
)

func ping(c *gin.Context) {
	c.String(http.StatusOK, "ok")
}

func main(){
	r := gin.Default()
	r.GET("/ping", ping)
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	v1 := r.Group("/v1")
	{
		v1.GET("/group", ping)
	}

	r.Run(":9090")
}





