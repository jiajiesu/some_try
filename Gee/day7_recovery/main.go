package main

import (
	"gee"
	"net/http"
)

func main() {
	r := gee.Default()
	r.GET("/", func(c *gee.Context) {
		c.String(http.StatusOK, "Hello Cristiano\n")
	})
	// index　out of range for testingRecovery()
	r.GET("/panic", func(c *gee.Context) {
		names := []string{"cristiano"}
		c.String(http.StatusOK, names[100])
	})

	r.Run(":9999")
}
