package gee

import "fmt"

/**
打印简单的信息
*/

func printNode(n *node) {
	fmt.Println("-----------node打印-----------")
	fmt.Println("pattern:", n.pattern)
	fmt.Println("part:", n.part)
	fmt.Println("isWild:", n.isWild)
	fmt.Println("children:", n.children)
	fmt.Println("-------------end-------------")
}
