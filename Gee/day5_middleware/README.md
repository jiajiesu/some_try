# 流程分析
## 初始化
1、New 一个engine, r

2、r.Use()添加中间件到对应的路由组

3、添加路由，构建路由树，Handler全部存在Router中的map，key为Get-Pattern，value为handler

4、r.Run, 将实现ServeHTTP

## 运行时
1、接收到请求，送到ServeHTTP解决

2、ServeHTTP:

    1、将engine.groups中的每个group.prefix与URL.Path比对前缀（路由层架的顺讯和添加中间件的顺序一样）;
    将每一个RouterGroup中的middlewares列表，复制给context的middlewares

    2、调用engine.router.handle(c),去执行Handler

3、engine.router.handle(c):
    
    1、取出匹配路由下的handler,加入context的handlers列表后

    2、c.Next()执行

4、c.Next(),根据先后添加的顺序执行handler