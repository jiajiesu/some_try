# some_try

## Introduction
一些技术上的尝试，比如**手写一颗路由树**、**尝试高并发的demo**、**简单复现一个框架** 等。

## router_tree
路由树实现

## Gee
参考 **gin** 框架，根据类似的逻辑，实现 **Gee** 框架

参考:
https://geektutu.com/post/gee.html

## GeeCache
参考 **gomemcache** 框架，实现 **GeeCache** 框架

参考：
https://geektutu.com/post/gee.html

## goroutine_pool
实现一个 **生产者消费者场景** 下的高并发demo

## file_about
操作文件相关的一些demo
  
## cli-command  
一个简单的命令行工具 - demo  
计划做个人词表  

## try_diversion_experiment
分层实验平台，相关的一些demo实现
