package main

import (
	"bytes"
	"math/rand"
)

func fib(n int) int {
	if n <= 1 {
		return 1
	}

	return fib(n-1) + fib(n-2)
}

func fib2(n int) int {
	if n <= 1 {
		return 1
	}

	f1, f2 := 1, 1
	for i := 2; i <= n; i++ {
		f1, f2 = f2, f1+f2
	}

	return f2
}

const Letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func generate(n int) string {
	var buf bytes.Buffer
	for i := 0; i < n; i++ {
		buf.WriteByte(Letters[rand.Intn(len(Letters))])
	}
	return buf.String()
}

func repeat(s string, n int) string {
	var result string
	for i := 0; i < n; i++ {
		result += s
	}

	return result
}

func main() {
	// ---- cpu -- runtime.profile
	// f, _ := os.OpenFile("cpu.profile", os.O_CREATE|os.O_RDWR, 0644)
	// defer f.Close()
	// pprof.StartCPUProfile(f)
	// defer pprof.StopCPUProfile()

	// n := 10
	// for i := 1; i <= 5; i++ {
	// 	fmt.Printf("fib(%d)=%d\n", n, fib(n))
	// 	n += 3 * i
	// }

	// ---- mem -- runtime.profile
	// f, _ := os.OpenFile("mem.profile", os.O_CREATE|os.O_RDWR, 0644)
	// defer f.Close()
	// for i := 0; i < 100; i++ {
	// 	repeat(generate(100), 100)
	// }
	// pprof.Lookup("heap").WriteTo(f, 0)

	// ---- cpu -- github.com/pkg/profile
	// defer profile.Start().Stop()
	// n := 10
	// for i := 1; i <= 5; i++ {
	// 	fmt.Printf("fib(%d)=%d\n", n, fib(n))
	// 	n += 3 * i
	// }

	// ---- mem -- github.com/pkg/profile
	// defer profile.Start(profile.MemProfile, profile.MemProfileRate(1)).Stop()
	// for i := 0; i < 100; i++ {
	// 	repeat(generate(100), 100)
	// }

	// ---- online cpu & mem -- "net/http/pprof"
	online()
}
