/*
 * @Author: crissu 13750782397@163.com
 * @Date: 2023-07-23 00:03:55
 * @LastEditors: crissu 13750782397@163.com
 * @LastEditTime: 2023-07-23 00:04:01
 * @FilePath: /try_pprof/benchmark_test.go
 * @Description:
 */
package main

import "testing"

func BenchmarkFib(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fib(30)
	}
}
