/*
 * @Author: crissu 13750782397@163.com
 * @Date: 2024-03-06 23:59:55
 * @LastEditors: crissu 13750782397@163.com
 * @LastEditTime: 2024-03-07 01:26:44
 * @FilePath: /try_diversion_experiment/main.go
 * @Description: 分层实验平台相关的一些尝试
 */
package main

import (
	"try_diversion_experiment/layer"
)

func main() {
	layer.LayerTest()
}
