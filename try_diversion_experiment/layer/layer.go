/*
 * @Author: crissu 13750782397@163.com
 * @Date: 2024-03-07 00:01:24
 * @LastEditors: crissu 13750782397@163.com
 * @LastEditTime: 2024-03-07 01:34:15
 * @FilePath: /try_diversion_experiment/layer/layer.go
 * @Description: 层
 */
package layer

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"time"
)

const BUCKET_NUM = 1000
const FLOW_CONF_KEY_BUCKETS = "buckets"
const FLOW_CONF_KEY_FLOWRATIO = "flow_ratio"
const FLOW_CONF_KEY_BUCKETNUM = "bucket_num"
const FLOW_CONF_KEY_SALT = "salt"

var ExpGroupList = make([]ExpGroup, 0)

type ExpGroup struct {
	SampleID     string
	BucketNumber int
	IsOccupy     bool
	LayerID      string
	DomainID     string
	BusinessID   string
}

func NewExpGroup(bucketNum int, layerID, domainID, businessID string, isOccupy bool) ExpGroup {
	return ExpGroup{
		BucketNumber: bucketNum,
		IsOccupy:     isOccupy,
		LayerID:      layerID,
		DomainID:     domainID,
		BusinessID:   businessID,
	}
}

func GetBusinessID() string {
	return "01"
}
func GetDomainID() string {
	return "10"
}

func GetLayerID(domainId, id string) string {
	return domainId + id
}
func PrintfCurExpGroupList() {
	fmt.Println("-----------ExpGroupList----print start-------")
	for _, item := range ExpGroupList {
		fmt.Println("expGroup:", item)
	}
	fmt.Println("-----------ExpGroupList----print end-------")
}

func CreateLayer() {
	layerID := GetLayerID(GetDomainID(), "01")
	for i := 0; i < BUCKET_NUM; i++ {
		expGroup := NewExpGroup(i, layerID, GetDomainID(), GetBusinessID(), false)
		ExpGroupList = append(ExpGroupList, expGroup)
	}
}

func getFreeExpGroupList(layerID, domainID, businessID string) []ExpGroup {
	freeExpGroupList := make([]ExpGroup, 0)
	for _, item := range ExpGroupList {
		if !item.IsOccupy && item.LayerID == layerID && item.BusinessID == businessID && item.DomainID == domainID {
			freeExpGroupList = append(freeExpGroupList, item)
		}
	}
	return freeExpGroupList
}

func updateExpGroupInfo(layerID, domainID, businessID, sampleID string, isOccupy bool, bucketNumber int) {
	i := 0
	var expGroupCpy ExpGroup
	for ; i < len(ExpGroupList); i++ {
		if ExpGroupList[i].LayerID == layerID && ExpGroupList[i].DomainID == domainID && ExpGroupList[i].BusinessID == businessID && ExpGroupList[i].BucketNumber == bucketNumber {
			expGroupCpy = ExpGroupList[i]
			break
		}
	}
	expGroupCpy.IsOccupy = isOccupy
	expGroupCpy.SampleID = sampleID
	ExpGroupList[i] = expGroupCpy
}

// allocketBucket 分配流量桶
func allocketBucket(exp Experiment, flowRatio float64) (bucketsList []int) { // 0.235
	bucketsList = make([]int, 0)
	bucketNums := int(flowRatio * BUCKET_NUM)
	freeExpGroupList := getFreeExpGroupList(exp.LayerID, exp.DomainID, exp.BusinessID)
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	r.Shuffle(len(freeExpGroupList), func(i, j int) {
		freeExpGroupList[i], freeExpGroupList[j] = freeExpGroupList[j], freeExpGroupList[i]
	})
	for _, item := range freeExpGroupList[0:bucketNums] {
		bucketsList = append(bucketsList, item.BucketNumber)
		updateExpGroupInfo(item.LayerID, item.DomainID, item.BusinessID, exp.SampleID, true, item.BucketNumber)
	}
	return
}

var ExperimentList = make([]Experiment, 0)

type Experiment struct {
	ExpID      string
	LayerID    string
	DomainID   string
	BusinessID string
	FlowConf   string
	SampleID   string
}

func GetSampleID(domainID, LayerID, ExpID string) string {
	return domainID + LayerID + ExpID
}

func GetExpID(id string) string {
	return id
}

// flowRatio 千分位
func CreateExperiment(expID string, flowRatio float64) {
	exp := Experiment{}
	exp.ExpID = GetExpID(expID)
	exp.BusinessID = GetBusinessID()
	exp.DomainID = GetDomainID()
	exp.LayerID = GetLayerID(exp.DomainID, "01")
	exp.SampleID = GetSampleID(exp.DomainID, exp.LayerID, exp.ExpID)
	flowInfo := make(map[string]interface{})
	flowInfo[FLOW_CONF_KEY_BUCKETS] = allocketBucket(exp, flowRatio)
	flowInfo[FLOW_CONF_KEY_BUCKETNUM] = 1000
	flowInfo[FLOW_CONF_KEY_FLOWRATIO] = flowRatio
	flowInfo[FLOW_CONF_KEY_SALT] = exp.LayerID
	flowConf, _ := json.Marshal(flowInfo)
	exp.FlowConf = string(flowConf)
	ExperimentList = append(ExperimentList, exp)
}

func PrintfCurExperimentList() {
	fmt.Println("-----------ExperimentList----print start-------")
	for _, item := range ExperimentList {
		fmt.Println("experiment:", item)
	}
	fmt.Println("-----------ExperimentList----print end-------")
}

// 测试千分位随机分桶
func LayerTest() {
	CreateLayer()
	CreateExperiment("01", 0.005)
	CreateExperiment("02", 0.002)
	PrintfCurExperimentList()
	PrintfCurExpGroupList()
}
