package performanceanalyse

import "testing"

func BenchmarkGenerateWithCap(b *testing.B) {
	for i:=0; i<b.N; i++{
		generateWithCap(1000000)
	}
}

func BenchmarkGenerate(b *testing.B) {
	for i:=0; i<b.N; i++{
		generate(1000000)
	}
}

func benchmarkGenerateCustom(n int, b *testing.B) {
	b.ResetTimer()
	for i:=0; i<b.N; i++{
		generate(n)
	}
}

func BenchmarkGenerate1000(b *testing.B)    { benchmarkGenerateCustom(1000, b) }
func BenchmarkGenerate10000(b *testing.B)   { benchmarkGenerateCustom(10000, b) }
func BenchmarkGenerate100000(b *testing.B)  { benchmarkGenerateCustom(100000, b) }
func BenchmarkGenerate1000000(b *testing.B) { benchmarkGenerateCustom(1000000, b) }









