# 基准测试
## code file
```text
fib.go
fib_test.go
generate.go
generate_test.go
sort.go
sort_test.go
```

## 学习参考
[benchmark 基准测试](https://geektutu.com/post/hpg-benchmark.html#3-2-StopTimer-amp-StartTimer)

## 简单总结
### 1、可以测试 1s（可自己设置）内可以执行多少次
```go
// 查看时间
BenchmarkFib-8                       100          10077936 ns/op
BenchmarkGenerateWithCap-8            84          14298489 ns/op
BenchmarkGenerate-8                   28          38781058 ns/op
BenchmarkGenerate1000-8            45157             25893 ns/op
BenchmarkGenerate10000-8            5959            192031 ns/op
BenchmarkGenerate100000-8            646           1853922 ns/op
BenchmarkGenerate1000000-8            27          38877247 ns/op
```
### 2、可以测试 内存使用情况
```go
// 查看时间 和 内存使用
BenchmarkFib-8                       123           9677450 ns/op               0 B/op          0 allocs/op
BenchmarkGenerateWithCap-8            72          14025799 ns/op         8003609 B/op          1 allocs/op
BenchmarkGenerate-8                   31          37899484 ns/op        45188485 B/op         41 allocs/op
BenchmarkGenerate1000-8            10000            100499 ns/op          174476 B/op         16 allocs/op
BenchmarkGenerate10000-8           12003            117355 ns/op          209824 B/op         17 allocs/op
BenchmarkGenerate100000-8          12013            117592 ns/op          209971 B/op         17 allocs/op
BenchmarkGenerate1000000-8         12004            117610 ns/op          209839 B/op         17 allocs/op
```
### 3、使用基准测试需要考虑的几个点
1、ResetTimer  
如果在 benchmark 开始前，需要一些准备工作，如果准备工作比较耗时，则需要将这部分代码的耗时忽略掉  
  
2、StopTimer & StartTimer  
如果测试一个冒泡函数的性能，每次调用冒泡函数前，需要随机生成一个数字序列，这是非常耗时的操作，这种场景下，就需要使用 StopTimer 和 StartTimer 避免将这部分时间计算在内。






