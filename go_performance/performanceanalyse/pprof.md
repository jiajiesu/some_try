# 基准测试
## code file
```text
pprofConcat.go
pprofMain.go
```

## 学习参考
[pprof 性能分析](https://geektutu.com/post/hpg-pprof.html)

## 简单总结
### 1、可以分析 程序对 cpu 的使用情况
根据瓶颈来改善代码

### 2、可以分析 程序对 mem 的使用情况


### 3、可以在 benchmark 中使用 pprof 分析 






