# 常用数据结构
## 字符串拼接性能
### 五种方法  
1 +  
2 fmt.Sprintf  
3 strings.Builder  
4 bytes.Buffer  
5 []byte

### 结论
strings.Builder 性能最好  
原理：中间切片的扩容、内粗分配次数减少  

## 切片性能及陷阱
### 数组和切片
数组：就是一块定长内存，创建时多大就多大，而且赋值的时候，只能是值赋值，属于深拷贝  
切片：动态数组，底层是一个结构体，一个指针指向一块内存，然后一个len和cap来描述这块内存，
赋值的时候是引用赋值
```go
nums := make([]int, 0, 8)
nums = append(nums, 1, 2, 3, 4, 5)
nums2 := nums[2:4]
printLenCap(nums)  // len: 5, cap: 8 [1 2 3 4 5]
printLenCap(nums2) // len: 2, cap: 6 [3 4]

nums2 = append(nums2, 50, 60)
printLenCap(nums)  // len: 5, cap: 8 [1 2 3 4 50]
printLenCap(nums2) // len: 4, cap: 6 [3 4 50 60]
```
切片还涉及到一个扩容机制

## 性能陷阱
```go
// 会导致大量内存得不到释放
func lastNumsBySlice(origin []int) []int {
	return origin[len(origin)-2:]
}

// 通过copy将小段内存复制出来，然后重建新切片，可以避免
func lastNumsByCopy(origin []int) []int {
	result := make([]int, 2)
	copy(result, origin[len(origin)-2:])
	return result
}

// 结果
=== RUN   TestLastCharsBySlice
--- PASS: TestLastCharsBySlice (0.37s)
slice_test.go:75: 100.14 MB
=== RUN   TestLastCharsByCopy
--- PASS: TestLastCharsByCopy (0.34s)
slice_test.go:76: 0.15 MB
```

