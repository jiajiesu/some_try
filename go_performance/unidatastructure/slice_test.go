package main

import (
	"runtime"
	"testing"
)

// printMem 用于打印程序运行时占用的内存大小
func printMem(t *testing.T){
	t.Helper()
	var rtm runtime.MemStats
	runtime.ReadMemStats(&rtm)
	t.Logf("%.2f MB", float64(rtm.Alloc)/1024./1024.)
}

func testLastChars(t *testing.T, f func([]int) []int){
	t.Helper()
	ans := make([][]int, 0)
	for k:=0; k<100; k++{
		origin := generateWithCap(128*1024) // 1M
		ans = append(ans, f(origin))
	}
	printMem(t)
	_ = ans
}

// testLastCharsGC 添加 gc
func testLastCharsGC(t *testing.T, f func([]int) []int){
	t.Helper()
	ans := make([][]int, 0)
	for k:=0; k<100; k++{
		origin := generateWithCap(128*1024) // 1M
		ans = append(ans, f(origin))
		runtime.GC()
	}
	printMem(t)
	_ = ans
}

//=== RUN   Test_lastNumsBySlice
//    slice_test.go:28: 100.15 MB
//--- PASS: Test_lastNumsBySlice (0.21s)
//func Test_lastNumsBySlice(t *testing.T) {
//	testLastChars(t, lastNumsBySlice)
//}

//=== RUN   Test_lastNumsByCopy
//    slice_test.go:32: 3.15 MB
//--- PASS: Test_lastNumsByCopy (0.20s)
//func Test_lastNumsByCopy(t *testing.T) {
//	testLastChars(t, lastNumsByCopy)
//}

// 增加 GC
//=== RUN   Test_lastNumsBySlice
//    slice_test.go:56: 100.14 MB
//--- PASS: Test_lastNumsBySlice (0.22s)
func Test_lastNumsBySlice(t *testing.T) {
	testLastCharsGC(t, lastNumsBySlice)
}

//=== RUN   Test_lastNumsByCopy
//    slice_test.go:60: 0.14 MB
//--- PASS: Test_lastNumsByCopy (0.20s)
func Test_lastNumsByCopy(t *testing.T) {
	testLastCharsGC(t, lastNumsByCopy)
}