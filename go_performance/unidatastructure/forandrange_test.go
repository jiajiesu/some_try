package main

import (
	"testing"
)

func BenchmarkForIntSlice(b *testing.B) {
	nums := generateWithCap(1024 * 1024)
	for i := 0; i < b.N; i++ {
		len := len(nums)
		var tmp int
		for k := 0; k < len; k++ {
			tmp = nums[k]
		}
		_ = tmp
	}
}

func BenchmarkRangeIntSlice(b *testing.B) {
	nums := generateWithCap(1024 * 1024)
	for i := 0; i < b.N; i++ {
		var tmp int
		for _, num := range nums {
			tmp = num
		}
		_ = tmp
	}
}

type Item struct{
	id int
	val [4096]byte
}

func BenchmarkForStruct(b *testing.B){
	var items [1024]Item
	for i:=0; i<b.N; i++{
		length := len(items)
		var tmp int
		for k:=0; k<length; k++{
			tmp = items[k].id
		}
		_ = tmp
	}
}

func BenchmarkRangeIndexStruct(b *testing.B){
	var items [1024]Item
	for i:=0; i<b.N; i++{
		var tmp int
		for k := range items{
			tmp = items[k].id
		}
		_ = tmp
	}
}
// BenchmarkRangeStruct 中 for _, item := range items, 是一段 值拷贝, items[i] 拷贝给 item，准确说是赋值
func BenchmarkRangeStruct(b *testing.B){
	var items [1024]Item
	for i:=0; i<b.N; i++{
		var tmp int
		for _, item := range items{
			tmp = item.id
		}
		_ = tmp
	}
}

func generateItems(n int) []*Item{
	items := make([]*Item, 0, n)
	for i:=0; i<n; i++{
		items = append(items, &Item{id: i})
	}
	return items
}

func BenchmarkForPointer(b *testing.B){
	items := generateItems(1024)
	for i:=0; i<b.N; i++{
		length := len(items)
		var tmp int
		for k:=0; k<length; k++{
			tmp = items[k].id
		}
		_ = tmp
	}
}

func BenchmarkRangePointer(b *testing.B){
	items := generateItems(1024)
	for i:=0; i<b.N; i++{
		var tmp int
		for k :=range items{
			tmp = items[k].id
		}
		_ = tmp
	}
}











