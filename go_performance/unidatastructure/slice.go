package main

import (
	"math/rand"
	"time"
)

// lastNumsBySlice 直接在原切片基础上进行切片
func lastNumsBySlice(origin []int) []int{
	return origin[len(origin)-2:]
}

// lastNumsByCopy 创建了一个新的切片，将 origin 的最后两个元素拷贝到新切片上，然后返回新切片
func lastNumsByCopy(origin []int) []int{
	result := make([]int, 2)
	copy(result, origin[len(origin)-2:])
	return result
}

// generateWithCap 用于随机生成 n 个 int 整数，64位机器上，一个 int 占 8 Byte，128 * 1024 个整数恰好占据 1 MB 的空间
func generateWithCap(n int) []int{
	rand.Seed(time.Now().UnixNano())
	nums := make([]int, 0, n)
	for i:=0; i<n; i++{
		nums = append(nums, rand.Int())
	}
	return nums
}



