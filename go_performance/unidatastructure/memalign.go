package main

type Args struct {
	num1 int
	num2 int
}

type Flag struct {
	num1 int16
	num2 int32
}

type demo1 struct {
	a int8
	b int16
	c int32
}

type demo2 struct {
	a int8
	c int32
	b int16
}

type demo3 struct {
	c int32
	a struct{}
}

type demo4 struct {
	a struct{}
	c int32
}

// 一个结构体实例所占据的空间等于各字段占据空间之和，再加上内存对齐的空间大小
func main() {
	//fmt.Println(unsafe.Sizeof(Args{}))
	//fmt.Println(unsafe.Sizeof(Flag{})) // num1(int16) 被内存对齐了
	//fmt.Println(unsafe.Alignof(Args{}))
	//fmt.Println(unsafe.Alignof(Flag{}))
	//fmt.Println(unsafe.Sizeof(demo1{})) // 8
	//fmt.Println(unsafe.Sizeof(demo2{})) // 12

	// 当 struct{} 作为其他 struct 最后一个字段时，需要填充额外的内存保证安全
	//fmt.Println(unsafe.Sizeof(demo3{})) // 8
	//fmt.Println(unsafe.Sizeof(demo4{})) // 4
}

