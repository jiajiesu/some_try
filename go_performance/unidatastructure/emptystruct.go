package main

// struct 不占据内存空间，因此被广泛作为各种场景下的占位符使用
//func main() {
//	fmt.Println(unsafe.Sizeof(struct {}{}))
//}

// 实现集合 Set
//type Set map[string]struct{}
//
//func (s Set)Has(key string) bool{
//	_, ok := s[key]
//	return ok
//}
//
//func (s Set)Add(key string) {
//	s[key] = struct{}{}
//}
//
//func (s Set)Delete(key string) {
//	delete(s, key)
//}
//
//func main() {
//	s := make(Set)
//	s.Add("Tim")
//	s.Add("CC")
//	fmt.Println(s.Has("Tim"))
//	fmt.Println(s.Has("jack"))
//}

// 不发送数据的 channel
//func worker(ch chan struct{}){
//	<-ch
//	fmt.Println("do something")
//	close(ch)
//}
//
//func main() {
//	ch := make(chan struct{})
//	go worker(ch)
//	ch <- struct{}{}
//}

// 仅包含方法的结构体
//type Door struct {
//
//}
//
//func (s Door) Open() {
//	fmt.Println("open the door")
//}
//
//func (s Door) Close() {
//	fmt.Println("close the door")
//}
//
//func main() {
//	door := Door{}
//	door.Open()
//	door.Close()
//}









