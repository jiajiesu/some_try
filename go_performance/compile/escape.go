package main

import "fmt"

type Demo struct {
	name string
}

func createDemo(name string) *Demo{
	d := new(Demo)
	d.name = name
	return d
}

// 指针逃逸
//func main() {
//	demo := createDemo("demo")
//	fmt.Println(demo)
//}

// interface{} 动态类型逃逸
//func main() {
//	demo := createDemo("demo")
//	test(demo)
//}
//
//func test(demo *Demo) {
//	fmt.Println(demo.name)
//}

// 栈空间不足
//func generate8176() {
//	nums := make([]int, 8176) // < 64KB
//	for i := 0; i < 8176; i++ {
//		nums[i] = rand.Int()
//	}
//}
//
//func generate8177() {
//	nums := make([]int, 8177) // = 64KB
//	for i := 0; i < 8177; i++ {
//		nums[i] = rand.Int()
//	}
//}
//
//func generate(n int) {
//	nums := make([]int, n) // 不确定大小
//	for i := 0; i < n; i++ {
//		nums[i] = rand.Int()
//	}
//}
//
//func main() {
//	generate8176()
//	generate8177()
//	generate(1)
//}

// 闭包
func increase() func() int {
	n := 0
	return func() int {
		n++
		return n
	}
}

func main() {
	in := increase()
	fmt.Println(in()) // 1
	fmt.Println(in()) // 2
}


