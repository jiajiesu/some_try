package main

import (
	"log"
	"net/http"
	"net/rpc"
)

type Result struct {
	Num int
	Ans int
}

type Cale int

func (calc *Cale)Square(num int, result *Result) error {
	result.Num = num
	result.Ans = num * num
	return nil
}

func main() {
	rpc.Register(new(Cale))
	rpc.HandleHTTP()

	log.Printf("Serving RPC server on port %d", 1234)
	if err := http.ListenAndServe(":1234", nil); err != nil{
		log.Fatal("Error serving: ", err)
	}
}