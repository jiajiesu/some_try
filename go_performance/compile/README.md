# 编译优化
## 减小编译体积
### 默认编译方式
go build -o server main.go
### 编译（忽略符号表和调试信息）
go build -ldflags="-s -w" -o server main.go  
- -s：忽略符号表和调试信息。  
- -w：忽略DWARFv3调试信息，使用该选项后将无法使用gdb进行调试。  

### 使用 upx 减小体积
**1、upx 压缩**  
brew install upx  

**2、仅使用 upx**  
go build -o server main.go && upx -9 server  

**3、 upx 和编译选项组合**  
go build -ldflags="-w -s" -o server main.go && upx -9 server  

**4、 upx的原理**  
upx 压缩后的程序和压缩前的程序一样，无需解压仍然能够正常地运行，这种压缩方法
称之为带壳压缩，压缩包含两个部分：  
- 在程序开头或其他合适的地方插入解压代码；  
- 将程序的其他部分压缩。   
    
执行时，也包含两个部分：  
- 首先执行的是程序开头的插入的解压代码，将原来的程序在内存中解压出来；  
- 再执行解压后的程序。  
  
tip: upx 在程序执行时，会有额外的解压动作，不过这个耗时几乎可以忽略。  
  

## 逃逸分析对性能对影响
### 背景
- 在函数中申请一个对象，如果分配在栈中，函数执行结束时自动回收，如果分配在堆中，
则在函数结束后某个时间点进行垃圾回收。
- 在栈上分配和回收内存的开销很低，只需要 2 个 CPU 指令：PUSH 和 POP，一个是
将数据 push 到栈空间以完成分配，pop 则是释放空间，也就是说在栈上分配内存，
消耗的仅是将数据拷贝到内存的时间，而内存的 I/O 通常能够达到 30GB/s，因此在
栈上分配内存效率是非常高的。
- 在堆上分配内存，一个很大的额外开销则是垃圾回收。Go 语言使用的是标记清除算法，
并且在此基础上使用了三色标记法和写屏障技术，提高了效率。  

### 什么是逃逸分析
在 C 语言中，可以使用 malloc 和 free 手动在堆上分配和回收内存。Go 语言中，
堆内存是通过垃圾回收机制自动管理的，无需开发者指定。那么，Go 编译器怎么知道某
个变量需要分配在栈上，还是堆上呢？编译器决定内存分配位置的方式，就称之为逃逸分
析(escape analysis)。逃逸分析由编译器完成，作用于编译阶段。  

### 指针逃逸
函数 createDemo 的局部变量 d 发生了逃逸。d 作为返回值，在 
main 函数中继续使用，因此 d 指向的内存不能够分配在栈上，随着
函数结束而回收，只能分配在堆上。  
```go
type Demo struct {
	name string
}

func createDemo(name string) *Demo {
	d := new(Demo) // 局部变量 d 逃逸到堆
	d.name = name
	return d
}
```

### interface{} 动态类型逃逸
在 Go 语言中，空接口即 interface{} 可以表示任意的类型，
如果函数参数为 interface{}，编译期间很难确定其参数的具
体类型，也会发生逃逸。  
demo 是 main 函数中的一个局部变量，该变量作为实参传递给 
fmt.Println()，但是因为 fmt.Println() 的参数类型定
义为 interface{}，因此也发生了逃逸。  
```go
func main() {
	demo := createDemo("demo")
	fmt.Println(demo)
}
```

### 栈空间不足
操作系统对内核线程使用的栈空间是有大小限制的，64 位系统上通常
是 8 MB。可以使用 ulimit -a 命令查看机器上栈允许占用的内存
的大小。  
对于 Go 语言来说，运行时(runtime) 尝试在 goroutine 需要
的时候动态地分配栈空间，goroutine 的初始栈大小为 2 KB。当 
goroutine 被调度时，会绑定内核线程执行，栈空间大小也不会超过
操作系统的限制。
```go
-t: cpu time (seconds)              unlimited
-f: file size (blocks)              unlimited
-d: data seg size (kbytes)          unlimited
-s: stack size (kbytes)             8176
-c: core file size (blocks)         0
-v: address space (kbytes)          unlimited
-l: locked-in-memory size (kbytes)  unlimited
-u: processes                       2666
-n: file descriptors                10240
```

### 闭包
Increase() 返回值是一个闭包函数，该闭包函数访问了外部变量 n，
那变量 n 将会一直存在，直到 in 被销毁。很显然，变量 n 占用的
内存不能随着函数 Increase() 的退出而回收，因此将会逃逸到堆上。
```go
func Increase() func() int {
	n := 0
	return func() int {
		n++
		return n
	}
}

func main() {
	in := Increase()
	fmt.Println(in()) // 1
	fmt.Println(in()) // 2
}
```


### 利用逃逸分析提升性能
传值会拷贝整个对象，而传指针只会拷贝指针地址，
指向的对象是同一个。传指针可以减少值的拷贝，
但是会导致内存分配逃逸到堆中，增加垃圾回收
(GC)的负担。在对象频繁创建和删除的场景下，
传递指针导致的 GC 开销可能会严重影响性能。  
- 对于需要修改原对象值，或占用内存比较大的结构体，选择传指针  
- 对于只读的占用内存较小的结构体，直接传值能够获得更好的性能。  


## 死码消除与调试模式
### 什么是死码消除
死码消除(dead code elimination, DCE)是一种编译器优化技术，
用处是在编译阶段去掉对程序运行结果没有任何影响的代码。  
死码消除有很多好处：减小程序体积，程序运行过程中避免执行无用的指
令，缩短运行时间。  

### 使用常量提升性能
在声明全局变量时，如果能够确定为常量，尽量使用 const 而非 var，
这样很多运算在编译器即可执行。死码消除后，既减小了二进制的体积，
又可以提高运行时的效率，如果这部分代码是 hot path，那么对性能的
提升会更加明显。  

### 可推断的局部变量
大小与 varconst 一致，即 a、b 作为局部变量时，编译器死码消除是生效的  
```go
// maxvarlocal
func main() {
	var a, b = 10, 20
	if max(a, b) == a {
		fmt.Println(a)
	}
}
```
如果再修改一下，函数中增加修改 a、b 变量的并发操作。  
此时，a、b 的值不能有效推断，死码消除失效。  
```go
func main() {
	var a, b = 10, 20
	go func() {
		b, a = a, b
	}()
	if max(a, b) == a {
		fmt.Println(a)
	}
}
```

### 调试(debug)模式
定义全局常量 debug，值设置为 false，在需要增加调
试代码的地方，使用条件语句 if debug 包裹，例如下
面的例子：  
```go
const debug = false

func main() {
	if debug {
		log.Println("debug mode is enabled")
	}
}
```








