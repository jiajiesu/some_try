// +build !debug
package main

import "log"

const debug = true

func main() {
	if debug {
		log.Println("debug mode is enabled")
	}
}



