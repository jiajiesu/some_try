package concurrence

import (
	"runtime"
	"testing"
	"time"
)

func testSelectFunc(){
	for i:=0; i<10; i++{
		//fmt.Println(selectFun())
		selectFun()
	}
}

func TestSelectFunc(t *testing.T) {
	for i:=0; i<10; i++{
		t.Log(runtime.NumGoroutine())
		//wg := &sync.WaitGroup{}
		//wg.Add(30)
		testSelectFunc()
		time.Sleep(2*time.Second)
		//wg.Wait()
		t.Log(runtime.NumGoroutine())
	}

}
