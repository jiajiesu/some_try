package concurrence

import (
	"fmt"
	"time"
)
// 如何退出协程（ 超时场景 ）

func doBadThing(done chan bool) {
	time.Sleep(time.Second)
	done<-true
}

func timeout(f func(chan bool)) error{
	done := make(chan bool)
	go f(done)
	select {
	case <-done:
		fmt.Println("done")
		return nil
	case <-time.After(time.Millisecond):
		return fmt.Errorf("timeout")
	}
}






