package concurrence

import (
	"github.com/Jeffail/tunny"
	"log"
	"time"
)

// 不控制协程数量，由于print打印需要将内容先写进一个文件描述符，容易导致内存爆炸
//func main() {
//	var wg sync.WaitGroup
//	for i:=0; i<math.MaxInt32; i++{
//		wg.Add(1)
//		go func(i int) {
//			defer wg.Done()
//			fmt.Println(i)
//			time.Sleep(time.Second)
//		}(i)
//	}
//	wg.Wait()
//}

// 利用 channel 的缓存区
//func main() {
//	var wg sync.WaitGroup
//	ch := make(chan struct{}, 3)
//	for i:=0; i<math.MaxInt32; i++{
//		ch<- struct{}{}
//		wg.Add(1)
//		go func(i int) {
//			defer wg.Done()
//			fmt.Println(i)
//			time.Sleep(time.Second)
//			<-ch
//		}(i)
//	}
//	wg.Wait()
//}


// 使用第三方库实现协程池，Jeffail/tunny
func main() {
	pool := tunny.NewFunc(3, func(i interface{}) interface{} {
		log.Println(i)
		time.Sleep(time.Second)
		return nil
	})
	defer pool.Close()

	for i:=0; i<10; i++{
		go pool.Process(i)
	}
	time.Sleep(time.Second*4)
}










