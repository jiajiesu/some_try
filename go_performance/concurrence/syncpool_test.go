package concurrence

import (
	"bytes"
	"encoding/json"
	"testing"
)

func BenchmarkUnmarshal(b *testing.B) {
	for n:=0; n<b.N; n++{
		stu := &Student{}
		json.Unmarshal(buf, stu)
	}
}

func BenchmarkUnmarshalWithPool(b *testing.B) {
	for n:=0; n<b.N; n++{
		stu := studentPool.Get().(*Student)
		json.Unmarshal(buf, stu)
		studentPool.Put(stu)
	}
}

func BenchmarkBufferWithPool(b *testing.B) {
	for n:=0; n<b.N; n++{
		buf := bufferPool.Get().(*bytes.Buffer)
		buf.Write(data)
	}
}

func BenchmarkBuffer(b *testing.B) {
	for n:=0; n<b.N; n++{
		var buf bytes.Buffer
		buf.Write(data)
	}
}





