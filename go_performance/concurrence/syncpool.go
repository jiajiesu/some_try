package concurrence

import (
	"bytes"
	"encoding/json"
	"sync"
)

type Student struct {
	Name string
	Age int32
	Remark [1024]byte
}

var buf, _ = json.Marshal(Student{
	Name:"Crissu",
	Age:25,
})

func unmarsh(){
	stu := &Student{}
	json.Unmarshal(buf, stu)
}

var studentPool = sync.Pool{
	New: func() interface{} {
		return new(Student)
	},
}


var bufferPool = sync.Pool{
	New: func() interface {}{
		return &bytes.Buffer{}
	},
}

var data = make([]byte, 10000)










