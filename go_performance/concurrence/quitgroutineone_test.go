package concurrence

import (
	"fmt"
	"log"
	"runtime"
	"testing"
	"time"
)

func test(t *testing.T, f func(chan bool)){
	t.Helper()
	for i:=0; i<1000; i++{
		timeout(f)
	}
	time.Sleep(time.Second*2)
	t.Log(runtime.NumGoroutine())
}

// 无缓冲channel
// 当超时发生时，select 接收到 time.After 的超时信号就返回了，
// done 没有了接收方(receiver)，而 doBadthing 在执行 1s 后
// 向 done 发送信号，由于没有接收者且无缓存区，发送者(sender)会
// 一直阻塞，导致协程不能退出。
//func TestBadTimeout(t *testing.T) {
//	test(t, doBadThing)
//}

func timeoutWithBuffer(f func(chan bool)) error{
	done := make(chan bool, 1)
	go f(done)
	select {
	case <-done:
		fmt.Println("done")
		return nil
	case <-time.After(time.Millisecond):
		return fmt.Errorf("timeout")
	}
}

// 有缓冲channel
// 协程数量下降为 2，创建的 1000 个子协程成功退出
//func TestBufferTimeout(t *testing.T) {
//	for i:=0; i<1000; i++{
//		timeoutWithBuffer(doBadThing)
//	}
//	time.Sleep(2*time.Second)
//	t.Log(runtime.NumGoroutine())
//}

func doGoodThing(done chan bool){
	time.Sleep(time.Second)
	select {
	case done <- true:
	default:
		return
	}
}

// 使用 select 尝试发送
// 使用 select 尝试向信道 done 发送信号，如果发送失败，
// 则说明缺少接收者(receiver)，即超时了，那么直接退出即可。
//func TestGoodTimeout(t *testing.T) {
//	test(t, doGoodThing)
//}

func do2phase(phase1, done chan bool){
	time.Sleep(time.Millisecond) // step 1
	select {
	case phase1<-true:
	default:
		return
	}
	fmt.Println("done1")
	time.Sleep(time.Millisecond*3) // step 2
	done<-true
	fmt.Println("done2")
}

func timeoutFirstPhase() error{
	phase1 := make(chan bool)
	done := make(chan bool)
	go do2phase(phase1, done)
	select {
	case <-phase1:
		<-done
		//fmt.Println("done")
		//log.Println("done")
		return nil
	case <-time.After(time.Second*2):
		log.Println("timeout")
		return fmt.Errorf("timeout")
	}
}

// 使用 select 做重试
// 这种情况下，就只能够使用 select，而不能能够设置缓冲区的方式了。
// 因为如果给信道 phase1 设置了缓冲区，phase1 <- true 总能执行
// 成功，那么无论是否超时，都会执行到第二阶段，而没有即时返回，这是
// 我们不愿意看到的。对应到上面的业务，就可能发生一种异常情况，向客
// 户端发送了 2 次响应
func Test2phaseTimeout(t *testing.T){
	for i:=0; i<1000; i++{
		timeoutFirstPhase()
	}
	time.Sleep(time.Second*3)
	t.Log(runtime.NumGoroutine())
}
