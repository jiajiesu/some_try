package concurrence

func selectFun() int {
	waitCh := make(chan int, 3)
	done := make(chan bool)
	go getResult(waitCh, done, 1)
	go getResult(waitCh, done, 2)
	go getResult(waitCh, done, 3)
	return <-waitCh
}

func getResult(ch chan int, done chan bool, i int) {
	select {
	case <-done:
		return
	case ch<-i:
		done<-true
	}
}


