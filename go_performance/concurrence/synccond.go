package concurrence

import (
	"fmt"
	"log"
	"sync"
	"time"
)

var done = false

func main() {
	startTime := time.Now()
	cond := sync.NewCond(&sync.Mutex{})

	go read("reader1", cond)
	go read("reader1", cond)
	go read("reader1", cond)

	write("writer", cond)

	time.Sleep(3*time.Second)
	fmt.Println(time.Since(startTime))
}

func write(name string, c *sync.Cond) {
	log.Println(name, " starts writing")
	time.Sleep(time.Second)
	c.L.Lock()
	done = true
	c.L.Unlock()
	log.Println(name, "wakes all")
	c.Broadcast()
}

func read(name string, c *sync.Cond) {
	c.L.Lock()
	for !done{
		c.Wait()
	}
	log.Println(name, " starts reading")
	c.L.Unlock()
}

