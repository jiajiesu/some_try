package concurrence

import (
	"log"
	"os"
	"strconv"
	"sync"
	"time"
)

type Config struct {
	Server string
	Port int64
}

var (
	once sync.Once
	config *Config
)

func ReadConfig() *Config{
	once.Do(func() {
		var err error
		config = &Config{Server: os.Getenv("TT_SERVER_URL")}
		config.Port, err = strconv.ParseInt(os.Getenv("TT_PORT"), 10, 0)
		if err != nil{
			config.Port = 8080 // default port
		}
		log.Println("init config, config:", config)
	})
	return config
}

// sync.Once 既能够保证全局变量初始化时是线程安全的，又能节省内存和初始化时间
// 适合初始化需要占用大量内存，消耗大量时间的场景
func main() {
	os.Setenv("TT_SERVER_URL", "crissu.top")
	os.Setenv("TT_PORT", "1010")
	for i:=0; i<10; i++{
		go func() {
			_ = ReadConfig()
		}()
	}
	time.Sleep(time.Second)
}











