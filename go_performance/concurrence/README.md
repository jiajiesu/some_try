# 并发编程
## 读写锁和互斥锁的性能比较

## 如何退出协程（超时场景）
`tip: 为了避免超时控制结束后，协程没有退出`
### 1、使用有缓冲区的channel
好处是：如果无缓冲，发送方会阻塞，直到接收方接收；如果有缓冲，在缓冲没有被用完的时候，发送方不会被阻塞  
  
### 2、使用select  
接收方必须已经在等了，这样就执行 case1;  
如果执行 case2, 说明控制超时方（timeout）那里已经超时了，不执行 <-done;  
这里可以用到 重发    
```go
func doGoodthing(done chan bool) {
	time.Sleep(time.Second)
	select {
	case done <- true:  // case 1, 通知上面结束了
	default:            // case 2
		return
	}
}
```
```go
func timeout(f func(chan bool)) error{
	done := make(chan bool)
	go f(done)
	select {
	case <-done:
		fmt.Println("done")
		return nil
	case <-time.After(time.Millisecond):
		return fmt.Errorf("timeout")
	}
}
```
```go
func test(t *testing.T, f func(chan bool)){
	t.Helper()
	for i:=0; i<1000; i++{
		timeout(f)
	}
	time.Sleep(time.Second*2)
	t.Log(runtime.NumGoroutine())
}
```
```go
func TestGoodTimeout(t *testing.T) {
	test(t, doGoodThing)
}
```
### 3、复杂场景
`将任务拆分为多段，只检测第一段是否超时，若没有超时，后续任务继续执行，超时则终止`
这里使用 select 方式来避免协程无法正常退出，如果使用 缓冲为1 的channel的方式，
则无论第一阶段是否正常完成，都会执行第二阶段；而需求是，如果第一阶段执行超时，则不用执行第二阶段。  

```go
func do2phases(phase1, done chan bool) {
	time.Sleep(time.Second) // 第 1 段
	select {
	case phase1 <- true:
	default:
		return
	}
	time.Sleep(time.Second) // 第 2 段
	done <- true
}

func timeoutFirstPhase() error {
	phase1 := make(chan bool)
	done := make(chan bool)
	go do2phases(phase1, done)
	select {
	case <-phase1:
		<-done
		fmt.Println("done")
		return nil
	case <-time.After(time.Millisecond):
		return fmt.Errorf("timeout")
	}
}

func Test2phasesTimeout(t *testing.T) {
	for i := 0; i < 1000; i++ {
		timeoutFirstPhase()
	}
	time.Sleep(time.Second * 3)
	t.Log(runtime.NumGoroutine())
}
```

### 能否强制 kill groutine？？？
不能，谢谢  


## 如何退出协程（其他场景）
### channel 忘记关闭的陷阱
`多个写 单个读 的情况下，解决 忘记关闭的channel还在监听的问题(因为关闭的channel也可以继续读)，这会导致协程无法正常退出`  
问题：  
```go
func do(taskCh chan int) {
	for {
		select {
		case t := <-taskCh:
			time.Sleep(time.Millisecond)
			fmt.Printf("task %d is done\n", t)
		}
	}
}

func sendTasks() {
	taskCh := make(chan int, 10)
	go do(taskCh)
	for i := 0; i < 1000; i++ {
		taskCh <- i
	}
}
```
解决：  
```go
func doCheckClose(taskCh chan int) {
	for {
		select {
		case t, beforeClosed := <-taskCh:
			if !beforeClosed {
				fmt.Println("taskCh has been closed")
				return
			}
			time.Sleep(time.Millisecond)
			fmt.Printf("task %d is done\n", t)
		}
	}
}

func sendTasksCheckClose() {
	taskCh := make(chan int, 10)
	go doCheckClose(taskCh)
	for i := 0; i < 1000; i++ {
		taskCh <- i
	}
	close(taskCh)
}
```

### 通道关闭的原则
1、粗鲁的方式（非常不推荐）  
```go
// 如果 channel 已经被关闭，再次关闭会产生 panic，这时通过 recover 使程序恢复正常。
func SafeClose(ch chan T) (justClosed bool) {
	defer func() {
		if recover() != nil {
			// 一个函数的返回结果可以在defer调用中修改。
			justClosed = false
		}
	}()

	// 假设ch != nil。
	close(ch)   // 如果 ch 已关闭，将 panic
	return true // <=> justClosed = true; return
}
```
2、礼貌的方式  
```go
// 使用 sync.Once 或互斥锁(sync.Mutex)确保 channel 只被关闭一次。
type MyChannel struct {
	C    chan T
	once sync.Once
}

func NewMyChannel() *MyChannel {
	return &MyChannel{C: make(chan T)}
}

func (mc *MyChannel) SafeClose() {
	mc.once.Do(func() {
		close(mc.C)
	})
}
```
3、 优雅的方式  
情形一：M个接收者和一个发送者，发送者通过关闭用来传输数据的通道来传递发送结束信号。  
情形二：一个接收者和N个发送者，此唯一接收者通过关闭一个额外的信号通道来通知发送者不要再发送数据了。  
情形三：M个接收者和N个发送者，它们中的任何协程都可以让一个中间调解协程帮忙发出停止数据传送的信号。  


## 控制协程的并发数量
`问题：当同时开启大量groutine之后，容易造成内存爆炸`
### 如何结局
**解决 1：使用channel的缓存区**  
每秒钟只并发执行了 3 个任务，达到了协程并发控制的目的
```go
// main_chan.go
func main() {
	var wg sync.WaitGroup
	ch := make(chan struct{}, 3)
	for i := 0; i < 10; i++ {
		ch <- struct{}{}
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			log.Println(i)
			time.Sleep(time.Second)
			<-ch
		}(i)
	}
	wg.Wait()
}
```

**解决 2：利用第三方库，协程池**  
```go
package main

import (
	"log"
	"time"

	"github.com/Jeffail/tunny"
)

func main() {
	pool := tunny.NewFunc(3, func(i interface{}) interface{} {
		log.Println(i)
		time.Sleep(time.Second)
		return nil
	})
	defer pool.Close()

	for i := 0; i < 10; i++ {
		go pool.Process(i)
	}
	time.Sleep(time.Second * 4)
}
```

### 可以手动调整系统资源的上限吗
当然可以  
**1、ulimit**  
操作系统通常会限制同时打开文件数量、栈空间大小等，ulimit -a 可以看到系统当前的设置：
```go
$ ulimit -a
-t: cpu time (seconds)              unlimited
-f: file size (blocks)              unlimited
-d: data seg size (kbytes)          unlimited
-s: stack size (kbytes)             8192
-c: core file size (blocks)         0
-v: address space (kbytes)          unlimited
-l: locked-in-memory size (kbytes)  unlimited
-u: processes                       1418
-n: file descriptors                12800
```
使用 ulimit -n 999999，将同时打开的文件句柄数量调整为 999999 来解决这个问题，其他的参数也可以按需调整。

**2、虚拟内存**
在 linux 上创建并使用交换分区是一件非常简单的事情：  
```go
sudo fallocate -l 20G /mnt/.swapfile # 创建 20G 空文件
sudo mkswap /mnt/.swapfile    # 转换为交换分区文件
sudo chmod 600 /mnt/.swapfile # 修改权限为 600
sudo swapon /mnt/.swapfile    # 激活交换分区
free -m # 查看当前内存使用情况(包括交换分区)
```
关闭交换分区也非常简单：
```go
sudo swapoff /mnt/.swapfile
rm -rf /mnt/.swapfile
```
如果应用程序只是在较短的时间内需要较大的内存，那么虚拟内存能够有效避免 out of memory 的问题。  
如果应用程序长期高频度读写大量内存，那么虚拟内存对性能的影响就比较明显了。


## sync.Pool 复用对象
### 使用场景
保存和复用临时对象，减少内存分配，降低 GC 压力  

### 如何使用
第一步, 申明对象池  
```go
var studentPool = sync.Pool{
    New: func() interface{} { 
        return new(Student) 
    },
}
```
第二步, Get & Put  
```go
stu := studentPool.Get().(*Student)
json.Unmarshal(buf, stu)
studentPool.Put(stu)
```
Get() 用于从对象池中获取对象，因为返回值是 interface{}，因此需要类型转换。  
Put() 则是在对象使用完毕后，返回对象池。  

### 扩展
1、printf 的源码中用到了 sync.pool 来减少对象创建，减少内存消耗  
2、[深度解密 Go 语言之 sync.Pool](https://www.cnblogs.com/qcrao-2018/p/12736031.html)


## sync.Once 如何提升性能
### 使用场景
sync.Once 是 Go 标准库提供的使函数只执行一次的实现，常应用于单例模式，例如初始化配置、保持数据库连接等。作用与 init 函数类似，但有区别。

init 函数是当所在的 package 首次被加载时执行，若迟迟未被使用，则既浪费了内存，又延长了程序加载时间。
sync.Once 可以在代码的任意位置初始化和调用，因此可以延迟到使用时再执行，并发场景下是线程安全的。
在多数情况下，sync.Once 被用于控制变量的初始化，这个变量的读写满足如下三个条件：

当且仅当第一次访问某个变量时，进行初始化（写）；
变量初始化过程中，所有读都被阻塞，直到初始化完成；
变量仅初始化一次，初始化完成后驻留在内存里。

### 原理
```go
package sync

import (
    "sync/atomic"
)

type Once struct {
    done uint32
    m    Mutex
}

func (o *Once) Do(f func()) {
    if atomic.LoadUint32(&o.done) == 0 {
        o.doSlow(f)
    }
}

func (o *Once) doSlow(f func()) {
    o.m.Lock()
    defer o.m.Unlock()
    if o.done == 0 {
        defer atomic.StoreUint32(&o.done, 1)
        f()
    }
}
```
**done 为什么是第一个字段**
```go
type Once struct {
    // done indicates whether the action has been performed.
    // It is first in the struct because it is used in the hot path.
    // The hot path is inlined at every call site.
    // Placing done first allows more compact instructions on some architectures (amd64/x86),
    // and fewer instructions (to calculate offset) on other architectures.
    done uint32
    m    Mutex
}
```
done 在热路径中，done 放在第一个字段，能够减少 CPU 指令，也就是说，这样做能够提升性能。  
简单解释下这句话：

1、热路径(hot path)是程序非常频繁执行的一系列指令，sync.Once 绝大部分场景都会访问 o.done，
在热路径上是比较好理解的，如果 hot path 编译后的机器码指令更少，更直接，必然是能够提升性能的。

2、为什么放在第一个字段就能够减少指令呢？因为结构体第一个字段的地址和结构体的指针是相同的，如果
是第一个字段，直接对结构体的指针解引用即可。如果是其他的字段，除了结构体指针外，还需要计算与第一
个值的偏移(calculate offset)。在机器码中，偏移量是随指令传递的附加值，CPU 需要做一次偏移值
与指针的加法运算，才能获取要访问的值的地址。因为，访问第一个字段的机器代码更紧凑，速度更快。


## sync.Cond 条件变量
`一个g 通知多个 g，相当于一排channel 或者 mutex`  
### 介绍
sync.Cond 条件变量用来协调想要访问共享资源的那些 goroutine，当共享资源的状态发生变化的时候，
它可以用来通知被互斥锁阻塞的 goroutine   
  
### 场景
有一个协程在异步地接收数据，剩下的多个协程必须等待这个协程接收完数据，才能读取到正确的数据。在这
种情况下，如果单纯使用 chan 或互斥锁，那么只能有一个协程可以等待，并读取到数据，没办法通知其他的
协程也读取数据。

这个时候，就需要有个全局的变量来标志第一个协程数据是否接受完毕，剩下的协程，反复检查该变量的值，直
到满足要求。或者创建多个 channel，每个协程阻塞在一个 channel 上，由接收数据的协程在数据接收完
毕后，逐个通知。总之，需要额外的复杂度来完成这件事。

### 使用
1、NewCond 创建实例  
NewCond 创建 Cond 实例时，需要关联一个锁。
```go
func NewCond(l Locker) *Cond
```
2、Broadcast 广播唤醒所有  
Broadcast 唤醒所有等待条件变量 c 的 goroutine，无需锁保护。
```go
func (c *Cond) Broadcast()
```
3、Signal 唤醒一个协程  
Signal 只唤醒任意 1 个等待条件变量 c 的 goroutine，无需锁保护。
```go
func (c *Cond) Signal()
```
4、Wait 等待  
调用 Wait 会自动释放锁 c.L，并挂起调用者所在的 goroutine，因此
当前协程会阻塞在 Wait 方法调用的地方。如果其他协程调用了 Signal 
或 Broadcast 唤醒了该协程，那么 Wait 方法在结束阻塞时，会重新
给 c.L 加锁，并且继续执行 Wait 后面的代码。
```go
func (c *Cond) Wait()
```



















