package concurrence

import (
	"runtime"
	"testing"
	"time"
)

// 单元测试执行结束后，子协程多了一个，也就是说，有一个协程一直没有得到释放。
// sendTasks 中启动了一个子协程 go do(taskCh)，因为这个协程一直处于阻塞
// 状态，等待接收任务，因此直到程序结束，协程也没有释放。
//func TestDo(t *testing.T) {
//	t.Log(runtime.NumGoroutine())
//	sendTasks()
//	time.Sleep(time.Second)
//	t.Log(runtime.NumGoroutine())
//}

func TestDoCheckClose(t *testing.T) {
	t.Log(runtime.NumGoroutine())
	sendTaskCheckClose()
	time.Sleep(time.Second)
	runtime.GC()
	t.Log(runtime.NumGoroutine())
}

