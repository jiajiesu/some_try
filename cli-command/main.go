package main

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var RootCommand = &cobra.Command{
	Use:   "cli",
	Short: "cli is demo for cobra",
	Run: func(cmd *cobra.Command, args []string) {

	},
}

var BuildSubCommand = &cobra.Command{
	Use: "build",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("do build")
		fmt.Println(cmd.Flag("build"))
	},
}

var TestSubCommand = &cobra.Command{
	Use: "test",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("do test")
		fmt.Println(args)
	},
}

// 字典查询
// ./cli-command dictory  --mode=query helloworld
var DictoryCommand = &cobra.Command{
	Use: "dictory",
	Run: func(cmd *cobra.Command, args []string) {
		mode := cmd.Flag("mode").Value.String()
		fmt.Println("mode:\t", mode)
		if mode == "set" {
			fmt.Println("key:\t", args[0])
			fmt.Println("value:\t", args[1])
		} else if mode == "query" {
			fmt.Println("key:\t", args[0])
		}
	},
}

func init() {
	RootCommand.AddCommand(BuildSubCommand)
	RootCommand.AddCommand(TestSubCommand)
	RootCommand.AddCommand(DictoryCommand)

	RootCommand.Flags().BoolP("update", "u", false, "更新")
	BuildSubCommand.Flags().BoolP("build", "b", true, "bbuildd")

	// 增加模式选择 flag
	DictoryCommand.Flags().String("mode", "set", "选择模式，查询or设置")
}

func main() {
	if err := RootCommand.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
