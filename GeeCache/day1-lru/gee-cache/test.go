/**
* @Author: Crissu
* @Date: 2021/4/24 21:30
# @About:
*/

package main

import (
	"fmt"
	"sync"
)

var m sync.Mutex
var set = make(map[int]bool, 0)

func printOnce(num int, wg *sync.WaitGroup) {
	defer wg.Done()
	m.Lock()
	if _, exist := set[num]; !exist {
		fmt.Println(num)
	}
	set[num] = true
	m.Unlock()
}

func main() {
	wg := sync.WaitGroup{}
	wg.Add(10)
	for i := 0; i < 10; i++ {
		go printOnce(100, &wg)
	}
	wg.Wait()
}
