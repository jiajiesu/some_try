module geecache

replace lru => ./lru

require lru v0.0.0

replace singleflight => ./singleflight

require singleflight v0.0.0

go 1.13
