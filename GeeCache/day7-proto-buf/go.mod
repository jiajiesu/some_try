module day7-proto-buf

replace lru => ./geecache/lru

require lru v0.0.0

replace geecache => ./geecache

require geecache v0.0.0

replace singleflight => ./geecache/singleflight

require singleflight v0.0.0

go 1.13
