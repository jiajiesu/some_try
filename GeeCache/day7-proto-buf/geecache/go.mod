module geecache

replace lru => ./lru

require lru v0.0.0

replace singleflight => ./singleflight

require (
	google.golang.org/protobuf v1.26.0 // indirect
	singleflight v0.0.0
)

go 1.13
