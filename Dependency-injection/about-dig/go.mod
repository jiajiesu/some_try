module about-dig

go 1.15

require (
	github.com/jessevdk/go-flags v1.5.0
	go.uber.org/dig v1.13.0
	gopkg.in/ini.v1 v1.66.3
)
