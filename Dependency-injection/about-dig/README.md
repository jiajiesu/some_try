# dig —— 一个依赖注入库

## 安装
```azure
go get go.uber.org/dig
go get gopkg.in/ini.v1
go get github.com/jessevdk/go-flags
```

## 一些问题
### 1、用来作什么的 ？
dig已经实现了DI容器，用户只需要将结构体和当前对象的执行交给dig,dig会自动的依赖注入和控制翻转
### 2、使用它什么好处 ？
1、使用DI容器的好处是分离当前对象和其依赖对象，将依赖对象的创建交给容器处理，这样用户只需要关心当前对象的执行就可以了； 
  
2、使用dig的好处是，dig可以快速创建一个di容器，只需按照dig的一些规则，将需要的依赖结构和执行函数进行注入即可。