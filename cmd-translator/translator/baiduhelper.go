package translator

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func generateMD5(b *baidu) string {
	str := strings.Builder{}
	str.WriteString(b.APPID)
	str.WriteString(b.RawText)
	str.WriteString(b.SALT)
	str.WriteString(b.KEY)
	h := md5.New()
	h.Write([]byte(str.String()))
	return hex.EncodeToString(h.Sum(nil))
}

func buildRequest(b *baidu) (*url.URL, error) {
	requestURl, err := url.Parse(b.URL)
	if err != nil {
		return nil, fmt.Errorf("baidu buildRequest err:%v", err)
	}
	params := url.Values{}
	params.Add("q", b.RawText)
	params.Add("from", b.from) // "en"
	params.Add("to", b.to)     // "zh"
	params.Add("appid", b.APPID)
	params.Add("salt", b.SALT)
	params.Add("sign", generateMD5(b))
	requestURl.RawQuery = params.Encode()
	return requestURl, nil
}

func requestForTranslate(requestURl string) ([]byte, error) {
	resp, err := http.Get(requestURl)
	if err != nil {
		log.Fatalln("requestForTranslate http.Get err:", err)
		return nil, fmt.Errorf("requestForTranslate http.Get err:%v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("requestForTranslate err, StatusCode=%v", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln("requestForTranslate ReadAll err:", err)
		return nil, err
	}
	return body, nil
}

func dealRespBody(respBody []byte) (string, error) {
	var result baiduResultBody
	err := json.Unmarshal(respBody, &result)
	if err != nil {
		log.Fatalln("baiduResultBody Unmarshal err:", err)
		return "", fmt.Errorf("dealRespBody baiduResultBody Unmarshal err:%v", err)
	}
	var trans_result []TransResult
	err = json.Unmarshal(result.TransResult, &trans_result)
	if err != nil {
		log.Fatalln("[]TransResult Unmarshal err:", err)
		return "", fmt.Errorf("dealRespBody []TransResult Unmarshal err:%v", err)
	}
	return trans_result[0].Dst, nil
}
