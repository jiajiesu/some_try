package translator

import (
	"cmd-translator/config"
	"fmt"
)

type baidu struct {
	RawText string
	from    string
	to      string
	URL     string
	APPID   string
	KEY     string
	SALT    string
}

// NewBaidu 创建 百度翻译器
func NewBaidu(config *config.Config) Translator {
	baiduTranslator := new(baidu)
	// 初始化加载参数
	baiduTranslator.LoadTranslatorConfig(config)
	return baiduTranslator
}

func (b *baidu) LoadTranslatorConfig(config *config.Config) {
	b.URL = config.Baidu.URL
	b.APPID = config.Baidu.APPID
	b.KEY = config.Baidu.KEY
	b.SALT = config.Baidu.SALT
}

func (b *baidu) TranslateFromSource() (string, error) {
	// 构建 url
	requestURl, err := buildRequest(b)
	if err != nil {
		return "", fmt.Errorf("buildRequest err:%v", err)
	}
	// 发起翻译请求
	respBody, err := requestForTranslate(requestURl.String())
	if err != nil {
		return "", fmt.Errorf("requestForTranslate err:%v", err)
	}
	// 处理返回的结果
	translateResult, err := dealRespBody(respBody)
	if err != nil {
		return "", fmt.Errorf("dealRespBody err:%v", err)
	}
	return translateResult, nil
}

func (b *baidu) SetQuery(query string) {
	b.RawText = query
}

func (b *baidu) SetFrom(from string) {
	b.from = from
}

func (b *baidu) SetTo(to string) {
	b.to = to
}
