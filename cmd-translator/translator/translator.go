package translator

import "cmd-translator/config"

type Translator interface {
	TranslateFromSource() (string, error)
	LoadTranslatorConfig(config *config.Config)
	SetQuery(query string)
	SetFrom(from string)
	SetTo(to string)
}
