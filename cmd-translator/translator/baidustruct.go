package translator

import "encoding/json"

type baiduResultBody struct {
	From        string          `json:"from"`
	To          string          `json:"to"`
	TransResult json.RawMessage `json:"trans_result"`
}

type TransResult struct {
	Src string `json:"src"`
	Dst string `json:"dst"`
}
