package translator

import (
	"fmt"
	"testing"
)

func Test_requestForTranslate(t *testing.T) {
	type args struct {
		requestURl string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			/*
				http://api.fanyi.baidu.
				com/api/trans/vip/translate?appid=20220127001068779&from=en&q=hello&salt=1435660288&sign
				=10386c297dd6dc33fa24264a2ea63693&to=zh
			*/
			name: "1",
			args: args{
				requestURl: "http://api.fanyi.baidu." +
					"com/api/trans/vip/translate?appid=20220127001068779&from=en&q=&salt=1234660288&sign" +
					"=ba00d27e21c5c23acf80ffbd91e3e323&to=zh",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := requestForTranslate(tt.args.requestURl)
			if (err != nil) != tt.wantErr {
				t.Errorf("requestForTranslate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			fmt.Println(string(got))
		})
	}
}
