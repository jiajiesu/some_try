package main

import (
	"cmd-translator/config"
	"cmd-translator/translator"
	"fmt"
	"strings"
)

func concatString(args []string) string {
	ret := strings.Builder{}
	for i, v := range args {
		ret.WriteString(v)
		if i != len(args)-1 {
			ret.WriteString(" ")
		}
	}
	return ret.String()
}

func buildBaiduTranslator(config *config.Config, rawText string, zhFlag string) translator.Translator {
	baiduTranslator := translator.NewBaidu(config)
	baiduTranslator.SetQuery(rawText)
	if zhFlag == "true" {
		baiduTranslator.SetFrom("zh")
		baiduTranslator.SetTo("en")
	} else {
		baiduTranslator.SetFrom("en")
		baiduTranslator.SetTo("zh")
	}
	return baiduTranslator
}

func showText(key, value string) {
	fmt.Println(key, ":\t", value)
}
