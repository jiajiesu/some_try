package main

import (
	"cmd-translator/config"
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"os"
)

const name = "sujiajie"

var RootCommand *cobra.Command

// 字典查询
func makeCommand(config *config.Config) *cobra.Command {
	return &cobra.Command{
		Use: "dic",
		Run: func(cmd *cobra.Command, args []string) {
			bFlag := cmd.Flag("baidu").Value.String()
			zhFlag := cmd.Flag("enOrZh").Value.String()
			if bFlag == "true" {
				// 使用百度翻译查询
				showText("当前翻译器", "百度翻译")
				rawText := concatString(args)
				showText("翻译内容", rawText)
				baiduTranslator := buildBaiduTranslator(config, rawText, zhFlag)
				result, err := baiduTranslator.TranslateFromSource()
				if err != nil {
					showText("翻译结果", "翻译失败")
					log.Fatalln("baiduTranslator.TranslateFromSource failed err:", err)
				}
				showText("翻译结果", result)
			}
		},
	}
}

func init() {
	// 加载全局配置
	cfg, err := config.LoadConfig()
	if err != nil {
		log.Fatalln("global LoadConfig err:", err)
		os.Exit(1)
	}

	// 初始化 Command
	RootCommand = makeCommand(cfg)
	// flag - 翻译器选择
	RootCommand.Flags().BoolP("baidu", "b", true, "使用百度翻译, 当前不管 -t 不 -t ，"+
		"都一样使用百度翻译")
	// flag - 中英文选择
	RootCommand.Flags().BoolP("enOrZh", "z", false, "默认（不输入）是英译中，-zh 是中译英")
}

func main() {
	if err := RootCommand.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
