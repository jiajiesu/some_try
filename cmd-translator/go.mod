module cmd-translator

go 1.15

require (
	github.com/BurntSushi/toml v1.0.0
	github.com/gobuffalo/envy v1.10.1 // indirect
	github.com/gobuffalo/packr v1.30.1 // indirect
	github.com/gobuffalo/packr/v2 v2.8.3 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/rogpeppe/go-internal v1.8.1 // indirect
	github.com/spf13/cobra v1.3.0
	golang.org/x/crypto v0.0.0-20220128200615-198e4374d7ed // indirect
	golang.org/x/sys v0.0.0-20220128215802-99c3d69c2c27 // indirect
)
