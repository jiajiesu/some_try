package config

type Config struct {
	Title string
	Baidu BaiduConfig
}

type BaiduConfig struct {
	URL   string
	APPID string
	KEY   string
	SALT  string
}

func LoadConfig() (*Config, error) {
	config := new(Config)
	config.Title = TITLE
	loadBaiduConfig(config)

	return config, nil
}

// 加载百度翻译配置
func loadBaiduConfig(config *Config) {
	config.Baidu = BaiduConfig{
		URL:   baiduURL,
		APPID: baiduAPPID,
		KEY:   baiduKEY,
		SALT:  baiduSALT,
	}
}
