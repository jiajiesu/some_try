# 压力测试

## pprof 分析协程
[Go -- pprof协程监控](https://www.cnblogs.com/mafeng/p/7323812.html)  

## 压力测试工具
[设计一个简单的压力测试工具](https://zhaoyang.me/posts/architecture-training-pressure-test-toy/)  
[code-wrk](https://github.com/wg/wrk)
```cmd
Usage：

wrk -t12 -c400 -d30s http://127.0.0.1:8080
This runs a benchmark for 30 seconds, using 12 threads, and keeping 400 HTTP connections open.

Output:

Running 30s test @ http://127.0.0.1:8080/index.html
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   635.91us    0.89ms  12.92ms   93.69%
    Req/Sec    56.20k     8.07k   62.00k    86.54%
  22464657 requests in 30.00s, 17.76GB read
Requests/sec: 748868.53
Transfer/sec:    606.33MB
```