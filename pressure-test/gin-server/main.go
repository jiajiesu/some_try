package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"runtime"
	"time"
)

// ./gin-server
func main() {

	g := gin.Default()
	g.GET("/", func(c *gin.Context) {
		log.Println("处理请求 - 开始")
		time.Sleep(time.Second * 3)
		log.Println(runtime.NumGoroutine()) // 打印当前协程数量
		c.String(200, "this pressure test")
		log.Println("处理请求 - 结束")
	})
	g.Run() // listen and serve on 0.0.0.0:8080
}
