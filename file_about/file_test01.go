/**
* @Author: Crissu
* @Date: 2021/2/2 13:08
# @About: 图片分片存储与分片读取
*/

package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func fileTest01() {
	r := gin.New()
	r.GET("/", handlerReadFile)       // 访问请求图片
	r.POST("/file", handlerStoreFile) // 上传图片
	r.Run(":8080")
}

// 返回
func returnMessage(c *gin.Context, code int, content string) {
	c.JSON(code, gin.H{"message": content})
}

// 保存分片
func saveBlock(name string, buf []byte, c *gin.Context) {
	save, err := os.OpenFile("./files/"+name, os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		returnMessage(c, 500, "文件保存出错")
	}
	defer save.Close() // 自动关闭文件句柄
	save.Write(buf)
}

// 分片 读取存储的图片
func handlerReadFile(c *gin.Context) {
	c.Writer.Header().Set("Transfer-Encoding", "chunked")
	c.Writer.Header().Set("Content-type", "image/png")
	for i := 0; i < 5; i++ {
		// 如果是分布式系统，读取文件就在这
		f, _ := os.Open(fmt.Sprintf("./files/img_%d.png", i))
		time.Sleep(time.Second) // 模拟延时
		b, _ := ioutil.ReadAll(f)
		c.Writer.Write(b)
		c.Writer.(http.Flusher).Flush()
	}
}

// 分片 存储上传的图片
func handlerStoreFile(c *gin.Context) {
	file, head, err := c.Request.FormFile("file")
	if err != nil {
		returnMessage(c, 400, "上传数据错误")
	}
	block := head.Size / 5
	index := 0
	for {
		buf := make([]byte, block)
		n, err := file.Read(buf)
		if err != nil && err != io.EOF {
			panic(err.Error())
		}
		if n == 0 {
			break
		}
		time.Sleep(time.Second) // 模拟延时
		saveBlock(fmt.Sprintf("img_%d.png", index), buf, c)
		index++
	}
	returnMessage(c, 200, "OK")
}
