/**
* @Author: Crissu
* @Date: 2021/2/8 17:11
# @About: 文件读取，显示进度
*/

package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"time"
)

// 设置格式，移动光标
const (
	Format1 = "\b\b%d%%"
	Format2 = "\b\b\b%d%%"
	Format3 = "\b\b\b%d%%\b"
)

// 显示进度
func progress(ch chan int64) {
	format := Format1
	var lastNum int64
	lastNum = 0
	for rate := range ch {
		if lastNum > 10 && rate > 10 && rate < 100 {
			format = Format2
		} else if rate >= 100 {
			rate = 100
			format = Format3
		}
		fmt.Printf(format, rate)
		lastNum = rate
	}
}

func fileTest02() {
	f, _ := os.Open("./files/img.jpg")
	finfo, _ := f.Stat()
	rateChan := make(chan int64)
	defer close(rateChan)
	fmt.Print("rate:0%")
	go progress(rateChan) // 显示进度
	ret := make([]byte, 0)
	for {
		buf := make([]byte, 1024*100)
		n, err := f.Read(buf)
		if err != nil && err != io.EOF {
			panic(err)
		}
		if n == 0 {
			break
		}
		ret = append(ret, buf...)
		time.Sleep(time.Second)
		// 写入进度
		go func() {
			rateChan <- int64(len(ret)*100) / finfo.Size()
		}()
	}
	ioutil.WriteFile("./files/ret.jpg", ret, 0600)
}
